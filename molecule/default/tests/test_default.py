import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_alerta_index(host):
    # This tests that traefik forwards traffic to the alerta web server
    # and that we can access the login page
    cmd = host.command(
        "curl -H Host:ics-ans-role-alerta-default -k -L https://localhost"
    )
    assert "<title>Alerta</title>" in cmd.stdout
