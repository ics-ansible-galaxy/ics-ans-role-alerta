# ics-ans-role-alerta

Ansible role to install alerta. An alert monitoring system that can de-duplicate and consolidate alerts from multiple sources.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
alerta_db_path: /opt/alerta/data
alerta_path: /opt/alerta
alerta_dirs:
  - "{{ alerta_db_path }}"
  - "{{ alerta_path }}"
alerta_db_user: postgres
alerta_db_pass: postgres
alerta_admin_users: admin@esss.se
alerta_network: alerta_network
alerta_container_name: alerta_web
alerta_image_name: alerta/alerta-web
alerta_image_tag: 7.1.0
alerta_db_container_name: alerta_db
alerta_db_image_name: postgres
alerta_db_image_tag: 11
alerta_hostname: "{{ ansible_fqdn }}"
alerta_login_domains: esss.se
alerta_debug: "False"
alerta_secret_key: changeme
alerta_plugins: reject,blackout,normalise,enhance
alerta_install_plugins: normalise,enhance,slack
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-alerta
```

## License

BSD 2-clause
